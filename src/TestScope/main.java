package TestScope;

import TestScope2.otherpackage;

public class main {
	
	String classstr = "Class";
	int classint = 123;
	
	public void methodA(){
		int methodint = 1234;
		String methodstr = "Method";
		System.out.println(methodstr);
		System.out.println(methodint);
		System.out.println(classstr);
		System.out.println(classint);
	}
	
	public static void main(String[] args){
		main test = new main();
		otherclass test2 = new otherclass();
		otherpackage test3 = new otherpackage();
		
		test.methodA();
		System.out.println("---------------");
		test2.methodA();
		System.out.println("---------------");
		test3.methodA();
		System.out.println("---------------");
	
		
	}
	


}

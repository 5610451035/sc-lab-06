package TestAbstractMethod;

public class Car extends Vehicle {


	public Car(String name, int price) {
		super(name, price);
	}

	@Override
	public int inSurance() {
		return 5;
	}

	@Override
	public String addFeul() {
		return "Benzine or NGV or LPG";
	}
	
	public int inSuranceimplement() {
		return 5;
	}

	
	public String addFeulimplement() {
		return "Benzine or NGV or LPG";
	}
	
	

}

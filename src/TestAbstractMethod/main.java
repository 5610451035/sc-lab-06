package TestAbstractMethod;

public class main {
	
	public static void main(String[] args) {
		
		Car car = new Car("Mercedes Benz :", 70000000);
		Motobike motor = new Motobike("Bigbike :", 4000000);
		ServiceCentre ser = new ServiceCentre();
		
		System.out.println(car.getName()+"\t"+car.addFeul());
		System.out.println("Quarantee: "+ser.checkInsurance(car));
		System.out.println(motor.getName()+"\t"+motor.addFeul());
		System.out.println("Quarantee: "+ser.checkInsurance(motor));
		
		
		System.out.println("-----------------------");
		
		
		System.out.println(car.getName()+"\t"+car.addFeulimplement());
		System.out.println("Quarantee: "+ser.checkInsurance(car));
		System.out.println(motor.getName()+"\t"+motor.addFeulimplement());
		System.out.println("Quarantee: "+ser.checkInsurance(motor));
	}
	
			
}

package TestAbstractMethod;

public abstract class Vehicle {
	
	private String name;
	private int price;
	
	public Vehicle(String name, int price) {
		this.name = name;
		this.price = price;
	}
	
	public String getName() {
		return this.name;
	}
	
	public abstract int inSurance();
	
	public abstract String addFeul();
	
}



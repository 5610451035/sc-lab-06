package TestAbstractMethod;

public class ServiceCentre {
	
	public int checkInsurance(Car c) {
		return c.inSurance();
	}
	
	public int checkInsurance(Motobike m) {
		return m.inSurance();
	}
	
	public int checkInsurance(Vehicle v) {
		return v.inSurance();
	}
	
	

}

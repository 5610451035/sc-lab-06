package TestSuperandSubclass;

public class Bird {
	
	private String name;
	
	 public Bird(String name){
		 System.out.println("Constructor SuperClass");
		 this.name=name;
	 }
	 
	 public String getName(){
		 return name;
	 }

	 public String flying(){
		 return "";
	 }
}
